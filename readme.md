# Thread aka Twitter


## Technologies

Backend:

* PHP 7.2 || 7.3
* [Laravel 5.8](https://laravel.com)
* [Docker](https://www.docker.com/)
* [Docker-compose](https://docs.docker.com/compose/)
* [Beanstalkd](https://github.com/beanstalkd/beanstalkd) - message queue (очередь сообщений для обработки тяжелых задач асинхронно)
* [REST API](https://ru.wikipedia.org/wiki/REST)
* [CORS](https://developer.mozilla.org/ru/docs/Web/HTTP/CORS)
* [JWT](https://ru.wikipedia.org/wiki/JSON_Web_Token) tokens
* Websockets and [Pusher](https://pusher.com/) service
* MySQL 5.7

Frontend:

* [Vue.js](https://vuejs.org/) framework
* [vue-cli](https://cli.vuejs.org/)
* [vue-router](https://router.vuejs.org/)
* [vuex](https://vuex.vuejs.org/)
* [Buefy](https://buefy.org/) UI framework
* [pusher-js](https://github.com/pusher/pusher-js)



